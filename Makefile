# Makefile for A2
# Ethan Morris
# 18/03/22

.SUFFIXES: .java .class
SRCDIR=src
BINDIR=bin
JAVAC=/usr/bin/javac
JAVA=/usr/bin/java

$(BINDIR)/%.class: $(SRCDIR)/%.java
	$(JAVAC) -d $(BINDIR)/ -cp $(BINDIR) $<

CLASSES=Vaccine.class \
	BinaryTreeNode.class \
	BTQueueNode.class \
        BTQueue.class \
        BinaryTree.class \
	AVLTree.class \
	AVLTreeTest.class \
	AVLExperiment.class
	
CLASS_FILE=$(CLASSES:%.class=$(BINDIR)/%.class)

default: $(CLASS_FILE)

run: $(CLASS_FILE)
	$(JAVA) -cp $(BINDIR) AVLExperiment
clean:
	rm $(BINDIR)/*.class