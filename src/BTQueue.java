// Hussein's Binary Tree
// 26 March 2017
// Hussein Suleman

/**
*@author MRRETH003
*/
public class BTQueue<Vaccine>
{   
   BTQueueNode<Vaccine> head;
   BTQueueNode<Vaccine> tail;
   
   /**
   * Instantiates BTQ object setting instant variables to null to start.
   */
   public BTQueue ()
   {
      head = null;
      tail = null;
   }
   
   /**
   * Retrieve next value from the queue.
   * @return BinaryTreeNode<Vaccine> node of the current head of the queue.
   */
   public BinaryTreeNode<Vaccine> getNext ()
   {
      if (head == null)
         return null;
      BTQueueNode<Vaccine> qnode = head;
      head = head.next;
      if ( head == null )
         tail = null;
      return qnode.node;
   }
   
   /**
   * Adds a new value to the end of the queue.
   * @param node Node to add to queue.
   */
   public void enQueue ( BinaryTreeNode<Vaccine> node )
   {
      if (tail == null)
      {   
         tail = new BTQueueNode<Vaccine> (node, null);
         head = tail;
      }   
      else
      {
         tail.next = new BTQueueNode<Vaccine> (node, null);
         tail = tail.next;
      }   
   }   
}
