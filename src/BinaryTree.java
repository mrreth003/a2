// Hussein's Binary Tree
// 26 March 2017
// Hussein Suleman

/**
* @author MRRETH003
*/
public class BinaryTree<Vaccine>
{
   public int opCount = 0;
   public int opCounter = 0;
   public int opSum = 0;
   public int opMax = 0;
   public int opMin = 100000000;
  
   BinaryTreeNode<Vaccine> root;
  
   /**
   * Instantiation of Binary Tree object
   */    
   public BinaryTree ()
   {
      root = null;
   }
   
   /**
   * Height of tree.
   * @return int Height of BT.
   */
   public int getHeight ()
   {
      return getHeight (root);
   }   
   
   /**
   * Height of tree - recursion.
   * @param node Current location checking.
   * @return int Height of BT or -1.
   */
   public int getHeight ( BinaryTreeNode<Vaccine> node )
   {
      if (node == null)
         return -1;
      else
         return 1 + Math.max (getHeight (node.getLeft ()), getHeight (node.getRight ()));
   }
   
   /**
   * Size of tree.
   * @return int Size of BT.
   */
   public int getSize ()
   {
      return getSize (root);
   }   
   
   /**
   * Size of tree using recursion.
   * @param node Current location checking.
   * @return int Size of BT or 0.
   */
   public int getSize ( BinaryTreeNode<Vaccine> node )
   {
      if (node == null)
         return 0;
      else
         return 1 + getSize (node.getLeft ()) + getSize (node.getRight ());
   }
   
   /**
   * Prints out data of node.
   * @param node Location of node to visit
   */
   public void visit ( BinaryTreeNode<Vaccine> node )
   {
      System.out.println (node.data);
   }
   
   /**
   * PreOrders the binary tree in order to make searching more efficient.
   */
   public void preOrder ()
   {
      preOrder (root);
   }
   
   /**
   * PreOrders the BST in order to make searching more efficient.
   * @param node Location of node to start preOrder.
   */
   public void preOrder ( BinaryTreeNode<Vaccine> node )
   {
      if (node != null)
      {
         visit (node);
         preOrder (node.getLeft ());
         preOrder (node.getRight ());
      }   
   }
   
   /**
   * PostOrders the BST in order to make searching more efficient.
   */
   public void postOrder ()
   {
      postOrder (root);
   }
   
   /**
   * PostOrders the BST to make searching more efficient.
   * @param node Location of node to start postOrder.
   */
   public void postOrder ( BinaryTreeNode<Vaccine> node )
   {
      if (node != null)
      {
         postOrder (node.getLeft ());
         postOrder (node.getRight ());
         visit (node);
      }   
   }

   /**
   * InOrders the BST to make searching more efficient.
   */
   public void inOrder ()
   {
      inOrder (root);
   }
   
   /**
   * InOrders the BST in order to make searching more efficient.
   * @param node Location of node to start inOrder.
   */
   public void inOrder ( BinaryTreeNode<Vaccine> node )
   {
      if (node != null)
      {
         inOrder (node.getLeft ());
         visit (node);
         inOrder (node.getRight ());
      }   
   }

   /**
   * LevelOrders the BST in order to make searching more efficient.
   */
   public void levelOrder ()
   {
      if (root == null)
         return;
      BTQueue<Vaccine> q = new BTQueue<Vaccine> ();
      q.enQueue (root);
      BinaryTreeNode<Vaccine> node;
      while ((node = q.getNext ()) != null)
      {
         visit (node);
         if (node.getLeft () != null)
            q.enQueue (node.getLeft ());
         if (node.getRight () != null)
            q.enQueue (node.getRight ());
      }
   }
}
