import java.io.*;
import java.util.*;

/**
* @author MRRETH003
*/
public class AVLExperiment
{
    AVLTree<Vaccine> avl = new AVLTree<Vaccine> ();
    
    /**
    * Function used throughout methods to write information to files with a specified name.
    * @param fileName Name of file to write to.
    * @param line String to be written to file.
    * @exception e Prevents error occurence when writing the data to a file.
    */
    public void writeToFile(String fileName, String line)
    {
         try 
         {
           File myObj = new File ("data/" + fileName);
//           myObj.createNewFile ();
           
           FileWriter myWriter = new FileWriter (myObj, true);
           myWriter.write(line + "\n");
           myWriter.close ();
         }
         catch (IOException e) 
         {
           System.out.println ("An error occurred when writing to the file.");
         }
    }

    /**
    * Read data into AVLTree from .csv file containing lines for Vaccine objects and randomising the data before insertion into the tree.
    * @param randomNum Number of randomisations to be done on file data.
    * @exception e Prevents an error when reading from file.
    */    
    public void readInFile (int randomNum)
    {
        String[] vax = new String[9919];
        
        // read content of file into data structure 
        File f = new File ("data/vaccinations.csv");
        try
        {
          Scanner sc = new Scanner (f);
          int i = 0;
          while (sc.hasNextLine ())
          {
              vax [i] = sc.nextLine ();
              i++;
          }
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }
        
        for (int i = 0; i < randomNum; i++)
        {
            Random rand = new Random ();
            int val1 = rand.nextInt (9919);
            int val2 = rand.nextInt (9919);
            
            String temp = new String ();
            temp = vax [val1];
            vax [val1] = vax [val2];
            vax [val2] = temp;
        }
        
        for (int i = 0; i < 9919; i++)
        {
            avl.insert (new Vaccine (vax [i]));
            writeToFile("randomVaccinations.txt", vax[i]);
            if (avl.opCounter > avl.opMax) 
              {
                  avl.opMax = avl.opCounter;
              }
            else if (avl.opCounter < avl.opMin)
              {
                  avl.opMin = avl.opCounter;
              }
            avl.opSum += avl.opCounter;
            avl.opCounter = 0;

        }
    }

    /**
    * Runs comparison between input file and AVLTree to test number of operations for each entry.
    * @param num The number of randomisations done before writing content to the AVLTree.
    */    
    public void search (int num)
    {
        File f = new File ("data/randomVaccinations.txt");
        try
        {
          Scanner sc = new Scanner (f);
          while (sc.hasNextLine ())
          {
              String line = sc.nextLine ();
              String c = new String();
              String d = new String();

              int i = line.indexOf(',');
              c = line.substring(0, i);
              d = line.substring(i + 1);
              i = d.indexOf(',');
              d = d.substring(0, i);

              Vaccine v = new Vaccine( c + "," + d + ",");
              AVLTree<Vaccine> avln = new AVLTree<Vaccine> ();
              avln.root = avl.find (v);
          }
          writeToFile("opMaxValues" + num + ".txt", String.valueOf (avl.opMax));
          writeToFile("opSumValue" + num + ".txt", String.valueOf (avl.opSum));
          writeToFile("opMinValue" + num + ".txt", String.valueOf (avl.opMin));
        }
        catch (Exception e)
        {
          //do something, report an error
          System.out.println ("An error occurred when reading the file");
        }
        
    }
    
    public static void main (String[] args)
    {
        AVLExperiment avle = new AVLExperiment ();
        avle.readInFile (Integer.parseInt(args[0]));
        avle.search (Integer.parseInt(args[0]));
    }
}