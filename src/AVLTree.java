// Hussein's AVL Tree
// 2 April 2017
// Hussein Suleman
// reference: kukuruku.co/post/avl-trees/

public class AVLTree<Vaccine extends Comparable<? super Vaccine>> extends BinaryTree<Vaccine>
{
   public int height ( BinaryTreeNode<Vaccine> node )
   {
      if (node != null)
         return node.height;
      return -1;
   }
   
   public int balanceFactor ( BinaryTreeNode<Vaccine> node )
   {
      return height (node.right) - height (node.left);
   }
   
   public void fixHeight ( BinaryTreeNode<Vaccine> node )
   {
      node.height = Math.max (height (node.left), height (node.right)) + 1;
   }
   
   public BinaryTreeNode<Vaccine> rotateRight ( BinaryTreeNode<Vaccine> p )
   {
      BinaryTreeNode<Vaccine> q = p.left;
      p.left = q.right;
      q.right = p;
      fixHeight (p);
      fixHeight (q);
      return q;
   }

   public BinaryTreeNode<Vaccine> rotateLeft ( BinaryTreeNode<Vaccine> q )
   {
      BinaryTreeNode<Vaccine> p = q.right;
      q.right = p.left;
      p.left = q;
      fixHeight (q);
      fixHeight (p);
      return p;
   }
   
   public BinaryTreeNode<Vaccine> balance ( BinaryTreeNode<Vaccine> p )
   {
      fixHeight (p);
      if (balanceFactor (p) == 2)
      {
         if (balanceFactor (p.right) < 0)
            p.right = rotateRight (p.right);
         return rotateLeft (p);
      }
      if (balanceFactor (p) == -2)
      {
         if (balanceFactor (p.left) > 0)
            p.left = rotateLeft (p.left);
         return rotateRight (p);
      }
      return p;
   }

   public void insert ( Vaccine v )
   {
      root = insert (v, root);
   }
   public BinaryTreeNode<Vaccine> insert ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      if (node == null)
         return new BinaryTreeNode<Vaccine> (v, null, null);
      opCounter++;
      if (v.compareTo (node.data) <= 0)
         node.left = insert (v, node.left);
      else
         node.right = insert (v, node.right);
      return balance (node);
   }
   
   public void delete ( Vaccine v )
   {
      root = delete (v, root);
   }   
   public BinaryTreeNode<Vaccine> delete ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      if (node == null) return null;
      opCount++;
      if (v.compareTo (node.data) < 0)
         node.left = delete (v, node.left);
      else if (v.compareTo (node.data) > 0)
      {
         opCount++;
         node.right = delete (v, node.right);
      }
      else
      {
         BinaryTreeNode<Vaccine> q = node.left;
         BinaryTreeNode<Vaccine> r = node.right;
         if (r == null)
            return q;
         BinaryTreeNode<Vaccine> min = findMin (r);
         min.right = removeMin (r);
         min.left = q;
         return balance (min);
      }
      return balance (node);
   }
   
   public BinaryTreeNode<Vaccine> findMin ( BinaryTreeNode<Vaccine> node )
   {
      if (node.left != null)
         return findMin (node.left);
      else
         return node;
   }

   public BinaryTreeNode<Vaccine> removeMin ( BinaryTreeNode<Vaccine> node )
   {
      if (node.left == null)
         return node.right;
      node.left = removeMin (node.left);
      return balance (node);
   }

   public BinaryTreeNode<Vaccine> find ( Vaccine v )
   {
      if (root == null)
         return null;
      else
         return find (v, root);
   }
   public BinaryTreeNode<Vaccine> find ( Vaccine v, BinaryTreeNode<Vaccine> node )
   {
      opCount++;
      if (v.compareTo (node.data) == 0) 
         return node;
      else if (v.compareTo (node.data) < 0)
      {
         opCount++;
         return (node.left == null) ? null : find (v, node.left);
      }
      else
      {
         return (node.right == null) ? null : find (v, node.right);
      }
   }
   
   public void treeOrder ()
   {
      treeOrder (root, 0);
   }
   public void treeOrder ( BinaryTreeNode<Vaccine> node, int level )
   {
      if (node != null)
      {
         for ( int i=0; i<level; i++ )
            System.out.print (" ");
         System.out.println (node.data);
         treeOrder (node.left, level+1);
         treeOrder (node.right, level+1);
      }
   }
}

