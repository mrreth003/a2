// Hussein's Binary Tree
// 26 March 2017
// Hussein Suleman

/**
* @author MRRETH003
*/
public class BinaryTreeNode<Vaccine>
{
   Vaccine data;
   BinaryTreeNode<Vaccine> left;
   BinaryTreeNode<Vaccine> right;
   int height;
   
   /**
   * Allocates Vaccine objectsto the parent and Node objects to the children baseed on the current node location.
   * @param v Vaccine object.
   * @param l Left child of the current node.
   * @param r Right child of the current node. 
   */   
   public BinaryTreeNode ( Vaccine v, BinaryTreeNode<Vaccine> l, BinaryTreeNode<Vaccine> r )
   {
      data = v;
      left = l;
      right = r;
      height = 0;
   }
   
   /**
   * Allows for access to left child node.
   * @return left Left child.
   */
   BinaryTreeNode<Vaccine> getLeft () { return left; }
   
   /**
   * Allows for access to right child node.
   * @return right Right child.
   */
   BinaryTreeNode<Vaccine> getRight () { return right; }
}
