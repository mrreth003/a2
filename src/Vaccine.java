/**
* Represents a Vaccine
* @author MRRETH003
*/

public class Vaccine implements Comparable<Vaccine>
{
    /**
    * This is a program for a that generates a Vaccine object and allows for the comparison of two said objects.
    */
    
    String country;
    String date;
    String vaccinations;
    
    /**
    * This method splits up the 3 parts of the line of information into a country name, date and number of vaccinations.
    * @params line The line containing country, date and vaccinations.
    */ 
    public Vaccine (String line)
    {
        String [] parts = line.split (",");
        /**
        * Splitting the line into the country, date and vaccinations for them to be stored in variables.
        */
        country = parts[0];
        date = parts[1];
        if (parts.length == 3)
        /**
        * Checking if vaccinations were mentioned in input and setting them to 0 if not.
        */
        
        {
            vaccinations = parts[2];
        }
        else
            vaccinations = "0";
    }
    
    /**
    * This method compares the string concatenated country and date of two countries in order to find a match.
    * @param v A Vaccine object.
    * @return An integer value representing the difference between Vaccine objects.
    */
    public int compareTo (Vaccine v)
    {        
         return (country+date).compareTo(v.country+v.date);
    }

}